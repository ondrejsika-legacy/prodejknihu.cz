import os

# BEGIN activacte virtualenv
import sys
from project.settings import PROJECT_ROOT, normpath

sys.path.append(normpath(PROJECT_ROOT))
sys.path.append(normpath(PROJECT_ROOT, "project"))

try:
    activate_path = normpath(PROJECT_ROOT, 'env/bin/activate_this.py')
    execfile(activate_path, dict(__file__=activate_path))
except IOError:
    print "E: virtualenv must be installed to PROJECT_ROOT/env"
# END activacte virtualenv

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()