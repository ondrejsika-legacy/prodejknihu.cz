from django import forms

from models import Vendor, Book, Category

class VendorForm(forms.ModelForm):
    class Meta:
        model = Vendor
        fields = ("username", "password", "first_name", "last_name", "email", "phone", "address")

class EditVendorForm(VendorForm):
    class Meta(VendorForm.Meta):
        exclude = ("password", )

class AboutMeForm(forms.ModelForm):
    class Meta:
        model = Vendor
        fields = ("about_me", )

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = ("vendor", "active", "slug_name", "slug_author", "short_url_coinurl")

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category