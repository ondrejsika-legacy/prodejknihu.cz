from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from django.db.models import Q
from django.contrib.auth.decorators import login_required

from models import Category, Book, Vendor
from forms import VendorForm, BookForm, AboutMeForm, EditVendorForm, CategoryForm
from django.contrib.auth.forms import  SetPasswordForm

DEBUG = False

def home_view(request, template="prodejknihu/home.html"):
    return render_to_response(template,
        {"category_list":Category.objects.all(),
         "all_books": Book.objects.all(),
         "all_vendors": Vendor.objects.all(), },
        context_instance=RequestContext(request))

def book_list_view(request, template="prodejknihu/book_list.html"):
    book_list = Book.objects.filter(active=True).order_by("-pk")

    try:
        s = request.GET["s"]
    except KeyError:
        s = None

    if s:
        book_list = book_list.filter(Q(name__contains=s) | 
                                Q(author__contains=s) |
                                Q(isbn=s) |
                                Q(vendor__username__contains=s) |
                                Q(category__name__contains=s )
                                )

    return render_to_response(template,
        {"book_list":book_list,
         "category_list":Category.objects.all(), },
        context_instance=RequestContext(request))

def vendor_list_view(request, template="prodejknihu/vendor_list.html"):
    vendor_list = Vendor.objects.all().order_by("-pk")
    return render_to_response(template,
        {"vendor_list": vendor_list, },
        context_instance=RequestContext(request))

def book_detail_view(request, book_pk, seo, template="prodejknihu/book_detail.html"):
    book = Book.objects.get(pk=book_pk)
    if not book.active:
        return HttpResponseRedirect("/")

    return render_to_response(template,
        {"book":book,
         "category_list":Category.objects.all(), },
        context_instance=RequestContext(request))

def vendor_detail_view(request, vendor_pk, template="prodejknihu/vendor_detail.html"):
    vendor = Vendor.objects.get(pk=vendor_pk)
    return render_to_response(template,
        {"vendor": vendor, },
        context_instance=RequestContext(request))

def register_view(request, template="prodejknihu/register.html"):
    form = VendorForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.set_password(obj.password)
        if DEBUG:
            obj.is_superuser = True
            obj.is_staff = True
        obj.save()
        return HttpResponseRedirect(reverse("prodejknihu.register_done"))

    return render_to_response(template,
        {"form": form, },
        context_instance=RequestContext(request))

@login_required
def settings_main_view(request, template="prodejknihu/settings_main.html"):
    vendor = request.user.vendor
    form = EditVendorForm(request.POST or None, instance=vendor)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse("prodejknihu.vendor_detail", args=[vendor.pk, ]))

    return render_to_response(template,
        {"form": form, },
        context_instance=RequestContext(request))

@login_required
def settings_about_me_view(request, template="prodejknihu/settings_about_me.html"):
    vendor = request.user.vendor
    form = AboutMeForm(request.POST or None, instance=vendor)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse("prodejknihu.vendor_detail", args=[vendor.pk, ]))

    return render_to_response(template,
        {"form": form, },
        context_instance=RequestContext(request))

@login_required
def settings_password_view(request, template="prodejknihu/settings_password.html"):
    vendor = request.user.vendor
    form = SetPasswordForm(vendor, request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse("prodejknihu.settings.password.done"))

    return render_to_response(template,
        {"form": form, },
        context_instance=RequestContext(request))

@login_required
def delete_book_view(request, book_pk, template="prodejknihu/delete_book.html"):
    book = Book.objects.get(pk=book_pk)
    if book.vendor != request.user.vendor:
        return HttpResponseRedirect("/")
    return render_to_response(template,
        {"book": book},
        context_instance=RequestContext(request))

@login_required
def delete_book_process(request, book_pk):
    book = Book.objects.get(pk=book_pk)
    if book.vendor != request.user.vendor:
        return HttpResponseRedirect("/")

    book.active = False
    book.save()
    return HttpResponseRedirect(reverse("prodejknihu.home"))

@login_required
def add_book_view(request, template="prodejknihu/add_book.html"):
    form = BookForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.vendor = request.user.vendor
        obj.save()
        return HttpResponseRedirect(reverse("prodejknihu.book_detail", args=[obj.pk, obj.get_seo_url()]))
    return render_to_response(template,
        {"form": form},
        context_instance=RequestContext(request))

@login_required
def add_category_view(request, template="prodejknihu/add_category.html"):
    form = CategoryForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse("prodejknihu.add_category_done"))
    return render_to_response(template,
        {"form": form},
        context_instance=RequestContext(request))

@login_required
def edit_book_view(request, book_pk, template="prodejknihu/edit_book.html"):
    book = Book.objects.get(pk=book_pk)
    if book.vendor != request.user.vendor:
        return HttpResponseRedirect("/")

    form = BookForm(request.POST or None, request.FILES or None, instance=book)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.vendor = request.user.vendor
        obj.save()
        return HttpResponseRedirect(reverse("prodejknihu.book_detail", args=[obj.pk, obj.get_seo_url()]))
    return render_to_response(template,
        {"form": form, "book": book, },
        context_instance=RequestContext(request))

def api_json_view(request):
    book_list = Book.objects.all()
    try:
        s = request.GET["s"]
    except KeyError:
        s = None
    if s:
        book_list = book_list.filter(Q(name__contains=s) | 
                                     Q(author__contains=s) |
                                     Q(isbn=s) |
                                     Q(vendor__username__contains=s) |
                                     Q(category__name__contains=s )
                                     )

    json_list = []
    for book in book_list:
        json_list.append(book.get_json())
    json_str = "[%s]"%", ".join(json_list)
    return HttpResponse(json_str)