from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from helpers import url2link, generate_short_url_coinurl

class Vendor(User):
    phone = models.CharField(max_length=32, null=True, blank=True)
    address = models.TextField(null=True, blank=True)

    about_me = models.TextField(null=True, blank=True)

    def get_active_book_list(self):
        return self.book_set.filter(active=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.username, self.get_full_name())

class Category(models.Model):
    name = models.CharField(max_length=128)

    def get_book_count(self):
        return self.book_set.filter(active=True).count()

    def __unicode__(self):
        return u"%s" % self.name

class Book(models.Model):
    vendor = models.ForeignKey(Vendor)
    created_at = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    category = models.ForeignKey(Category)
    name = models.CharField(max_length=128)
    slug_name = models.SlugField(max_length=128, blank=True)
    author = models.CharField(max_length=64)
    slug_author = models.CharField(max_length=64, blank=True)
    isbn = models.CharField(max_length=32)
    publisher = models.CharField(max_length=64)
    release_year = models.IntegerField(max_length=4)

    description = models.TextField(null=True, blank=True)
    img = models.ImageField(null=True, blank=True, upload_to="prodejknihu/boook")

    price = models.IntegerField()
    postage = models.IntegerField()

    short_url_coinurl = models.URLField(blank=True, null=True)

    def get_short_url_coinurl(self):
        if not self.short_url_coinurl:
            self.short_url_coinurl = generate_short_url_coinurl("/kniha/%s/%s/%s/"%(self.pk, self.slug_author, self.slug_name))
            self.save()
        return self.short_url_coinurl

    def get_seo_url(self):
        return u"%s/%s" % (self.slug_author, self.slug_name)

    def get_json(self):
        return '{"name":"%(name)s", "author":"%(author)s", "isbn":"%(isbn)s", "publisher": "%(publisher)s", "release_year":"%(release_year)s", "vendor":{"name": "%(vendor_name)s", "email": "%(vendor_email)s", "phone": "%(vendor_phone)s"}, "price": "%(price)s", "postage": "%(postage)s"}'%{"name":self.name, "author":self.author, "isbn": self.isbn, "publisher": self.publisher, "release_year": self.release_year, "description": self.description, "vendor_name": self.vendor.get_full_name(), "vendor_email": self.vendor.email, "vendor_phone": self.vendor.phone, "price": self.price, "postage":self.postage, }

    def get_html_title(self):
        title = [self.name]
        if len(self.author) > 2:
            title.append(self.author)
        return " | ".join(title)

    def get_description(self):
        return url2link(self.description)

    def __unicode__(self):
        return u"%s ~%s" % (self.name, self.vendor)

    def save(self, *args, **kwargs):
        self.slug_author = slugify(self.author)
        self.slug_name = slugify(self.name)
        return super(Book, self).save(*args, **kwargs)