from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView, TemplateView

from django.contrib.auth.views import login, logout
from views import *

urlpatterns = patterns("",
    url(r'^accounts/profile/', RedirectView.as_view(url="/")),

    url(r"^$",
        home_view,
        name="prodejknihu.home", ),

    url(r"^knihy/$",
        book_list_view,
        name="prodejknihu.book_list", ),

    url(r'^kniha/(?P<book_pk>\d+)/(?P<seo>[a-zA-Z0-9-/]+)/$',
         book_detail_view,
         name="prodejknihu.book_detail", ),

    url(r'^register/$',
         register_view,
         name="prodejknihu.register", ),

    url(r'^pridat-knihu/$',
         add_book_view,
         name="prodejknihu.add_book", ),

    url(r'^pridat-kategorii/$',
         add_category_view,
         name="prodejknihu.add_category", ),

    url(r'^pridat-kategorii/dokonceno/$',
         TemplateView.as_view(template_name="prodejknihu/add_category_done.html"),
         name="prodejknihu.add_category_done", ),

    url(r'^upravit-knihu/(?P<book_pk>\d+)/$',
         edit_book_view,
         name="prodejknihu.edit_book", ),

    url(r'^prodejci/$',
         vendor_list_view,
         name="prodejknihu.vendor.list", ),

    url(r'^detail-prodejce/(?P<vendor_pk>\d+)/$',
         vendor_detail_view,
         name="prodejknihu.vendor_detail", ),

    url(r'^smazat-knihu/(?P<book_pk>\d+)/$',
         delete_book_view,
         name="prodejknihu.delete_book", ),

    url(r'^nastaveni/o-mne/$',
         settings_about_me_view,
         name="prodejknihu.settings.about_me", ),

    url(r'^nastaveni/hlavni/$',
         settings_main_view,
         name="prodejknihu.settings.main", ),

    url(r'^nastaveni/zmena-hesla/$',
         settings_password_view,
         name="prodejknihu.settings.password", ),

    url(r'^nastaveni/zmena-hesla/done/$',
        TemplateView.as_view(template_name="prodejknihu/settings_password_done.html"),
        name="prodejknihu.settings.password.done", ),

    url(r'^smazat-knihu/(?P<book_pk>\d+)/process/$',
         delete_book_process,
         name="prodejknihu.delete_book.process", ),

    url(r'^register-done/$',
        TemplateView.as_view(template_name="prodejknihu/register_done.html"),
        name="prodejknihu.register_done", ),

    url(r'^register/$',
         register_view,
         name="prodejknihu.login", ),

    url(r'^accounts/login/$',
        login,
        {"template_name": "prodejknihu/login.html"},
        name="prodejknihu.login", ),

    url(r'^logout/$',
        logout,
        {"template_name": "prodejknihu/logout.html"},
        name="prodejknihu.logout", ),

    url(r'^api/$',
        TemplateView.as_view(template_name="prodejknihu/api.html"),
        name="prodejknihu.api", ),

    # api
    url(r'^api/json/$',
        api_json_view,
        name="prodejknihu.api.json", ),
)