import re

import requests

from django.conf import settings

def url2link(urlstr):
    try:
        url = re.search("(?P<url>https?://[^\s]+)", urlstr).group("url")
        return urlstr.replace(url, '<a href="%s" target="blank">%s</a>'%(url, url, ))
    except AttributeError:
        return urlstr

def generate_short_url_coinurl(path):
    uri = "https://coinurl.com/api.php?uuid=%s&url=http://prodejknihu.cz%s" % (settings.COINURL_UUID, path)
    return requests.get(uri).text
